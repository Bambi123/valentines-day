import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { ValentinesDayApp } from './valentines-day-app/valentines-day-app'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <ValentinesDayApp />
  </React.StrictMode>
);