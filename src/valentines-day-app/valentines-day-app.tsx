import { ReactElement, useState } from 'react';
import { ValentinesStateContext } from './context';
import { QuestionState, AreYouSureState, ThankYouState } from './states';

import './valentines-day-app.css';

export function ValentinesDayApp(): ReactElement {

  const [state, setState] = useState<'question' | 'are-you-sure' | 'thank-you'>('question');


  return (
    <ValentinesStateContext.Provider value={{ state, setState }}>
      <div className="valentines-day-app">
        {fetchComponentFromState()}
      </div>
    </ValentinesStateContext.Provider>
  );

  function fetchComponentFromState(): ReactElement {
    switch (state) {
      case 'question':
        return <QuestionState />;
      case 'are-you-sure':
        return <AreYouSureState />;
      case 'thank-you':
        return <ThankYouState />;
    }
  }
}


