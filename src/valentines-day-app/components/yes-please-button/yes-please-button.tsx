import { ReactElement } from "react";

import './yes-please-button.css';

export interface IYesPleaseButton {
    yesPleaseOnClick: () => void;
    customHeightScalar: number;
    classNames: string;
}

export function YesPleaseButton({ yesPleaseOnClick, customHeightScalar, classNames }: IYesPleaseButton): ReactElement {

    const customHeight = customHeightScalar * 30 + 30;
    const customWidth = customHeightScalar * 125 + 125;

    return <div
        className={`${classNames} yes-please`}
        onClick={yesPleaseOnClick}
        style={{ height: `${customHeight}px`, width: `${customWidth}px` }}
    >
        YES PLEASE
    </div>
}