import { ReactElement } from "react";

import './no-thanks-button.css';

export interface INoThanksButton {
    noThanksOnClick: () => void;
    classNames: string;
}

export function NoThanksButton({ noThanksOnClick, classNames }: INoThanksButton): ReactElement {
    return < div className={`${classNames} no-thanks`} onClick={noThanksOnClick} > NO THANKS</div >
}