import { createContext, useState } from 'react';

export interface IValentinesStateContext {
    state?: 'question' | 'are-you-sure' | 'thank-you';
    setState?: (str: 'question' | 'are-you-sure' | 'thank-you') => void;
}

export const ValentinesStateContext = createContext<IValentinesStateContext>({})