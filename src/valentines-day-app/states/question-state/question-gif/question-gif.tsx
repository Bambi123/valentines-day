import { ReactElement } from "react";

export function QuestionGif(): ReactElement {
    return <iframe src="https://giphy.com/embed/3o7TKMt1VVNkHV2PaE" className="are-you-sure-gif" />
}