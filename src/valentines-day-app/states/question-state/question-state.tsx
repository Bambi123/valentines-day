import { ReactElement, useContext, useState } from "react";

import './question-state.css';
import { QuestionGif } from "./question-gif/question-gif";
import { IValentinesStateContext, ValentinesStateContext } from "../../context";
import { NoThanksButton, YesPleaseButton } from "../../components";

export function QuestionState(): ReactElement {

    const [counter, setCounter] = useState<number>(0);

    const { setState } = useContext<IValentinesStateContext>(ValentinesStateContext);

    function yesPleaseOnClick(): void {
        if (setState) setState('thank-you')
    }

    function noThanksOnClick(): void {
        setCounter(counter + 1);
        if (counter === 10 && setState) setState('are-you-sure');
    }

    return <div className="question-state">
        <div className="hi-booby">Hi bubba</div>
        <QuestionGif />
        <div className='the-question'>Will you be my valentine?</div>
        <div className='button-wrapper'>
            <YesPleaseButton yesPleaseOnClick={yesPleaseOnClick} customHeightScalar={counter} classNames={'button'} />
            <NoThanksButton noThanksOnClick={noThanksOnClick} classNames={'button'} />
        </div>
    </div>
}