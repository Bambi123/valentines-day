import { ReactElement } from "react";
import './thank-you-gif.css';

export function ThankYouGif(): ReactElement {
    return <iframe src="https://giphy.com/embed/KztT2c4u8mYYUiMKdJ" className="thank-you-gif" />
}