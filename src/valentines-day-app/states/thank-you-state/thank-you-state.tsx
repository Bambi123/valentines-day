import { ReactElement } from "react";
import { ThankYouGif } from "./thank-you-gif";

import './thank-you-state.css';

export function ThankYouState(): ReactElement {
    return <div className="thank-you-state">
        <div className='ur-the-cutest' >{'ur the cutest :)'}</div>
        <ThankYouGif />
        <div className="happy-valentines-day">Happy Valentines's day GORL</div>
    </div>
}