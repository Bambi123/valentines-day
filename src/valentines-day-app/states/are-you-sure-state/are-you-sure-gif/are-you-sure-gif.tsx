import { ReactElement } from "react";

import './are-you-sure-gif.css';

export function AreYouSureGif(): ReactElement {
    return <iframe src="https://giphy.com/embed/djnAy9p1lwi5Swhtqa" className="are-you-sure-gif" />
}