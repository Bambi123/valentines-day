import { ReactElement, useState, useContext } from "react";
import { IValentinesStateContext, ValentinesStateContext } from "../../context";
import { NoThanksButton, YesPleaseButton } from "../../components";
import { AreYouSureGif } from "./are-you-sure-gif";

import './are-you-sure.css';

const initialTitle = 'Are you sure :('
const titles = [initialTitle,
    'Why not???',
    "I can't believe this",
    "...",
    'unbelievable',
    "you can stop it now!",
    "I know you love me",
    "press no if you love me",
    "HAH! got you!",
    "and you're still pressing it...",
    "do you really hate me that much?",
    "okay well I guess...",
    "...I'll just have to go hang out with the homies",
    "just kidding!",
    "but seriously, it's time to stop",
    'and love me',
    "this text prompt won't go forever",
    "I have other things to do you know",
    "asjfajhsbgkefvaekufevfkeu",
    "....",
    "you must really hate me :(("
]

export function AreYouSureState(): ReactElement {

    const { setState } = useContext<IValentinesStateContext>(ValentinesStateContext);

    const [title, setTitle] = useState<string>(initialTitle)
    const [counter, setCounter] = useState<number>(1);
    const [hide, setHide] = useState<boolean>(false);

    function yesPleaseOnClick(): void {
        if (setState) setState('thank-you')
    }

    function noThanksOnClick(): void {
        setCounter(counter + 1);
        changeTitle();
    }

    return <div className='are-you-sure-state'>
        <div className="are-you-sure">{title}</div>
        <AreYouSureGif />
        <div className="button-wrapper">
            <YesPleaseButton classNames='button' yesPleaseOnClick={yesPleaseOnClick} customHeightScalar={hide ? 5 : 0} />
            <NoThanksButton classNames={`button ${hide ? 'hidden' : ''}`} noThanksOnClick={noThanksOnClick} />
        </div>
    </div>

    function changeTitle(): void {
        if (counter >= titles.length - 1) {
            setTitle('...I still love you anyway <3');
            setHide(true);
        }
        else setTitle(titles[counter]);
    }
}

